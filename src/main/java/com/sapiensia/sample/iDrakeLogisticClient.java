package com.sapiensia.sample;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sapiensia.sample.valueObjects.MessageType;
import com.sapiensia.sample.valueObjects.Provider;
import com.sapiensia.sample.utils.LocalDateTimeDeserializer;
import com.sapiensia.sample.utils.LocalDateTimeSerializer;
import com.sapiensia.sample.messages.*;
import com.sapiensia.sample.utils.SecurityHelper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

public class iDrakeLogisticClient {

    private Gson gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
            .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer())
            .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer())
            .create();

    private String endpoint;
    private String privateKey;
    private int myId;

    public iDrakeLogisticClient(String endpoint, String privateKey, int myId) {
        this.endpoint = endpoint;
        this.privateKey = privateKey;
        this.myId = myId;
    }

    public List<Message> readMessages() throws Exception {

        GetUnconfirmedMessage cmd = new GetUnconfirmedMessage();
        cmd.recipientId = this.myId;

        ArrayList<GetUnconfirmedMessage> cmds = new ArrayList<GetUnconfirmedMessage>();
        cmds.add(cmd);

        // [{"$id":"1","Id":0,"ClientGuid":"92392d0e-28e4-4527-a6fd-282e80a79b95","RecipientId":96,"Type":12,"MessageIdsToRead":[]}]
        String response = request(cmds, "Receive");

        System.out.println(response);

        ServiceMessage[] serviceMessages = this.gson.fromJson(response, ServiceMessage[].class);

        ArrayList<Message> result = new ArrayList<>();

        for(ServiceMessage sm : serviceMessages) {
            if (sm.type == MessageType.Acknowledgment.code) {
                result.add(new Acknowledgment(sm));
            }
            else if (sm.type == MessageType.ConfirmProcessing.code) {
                result.add(new ConfirmProcessing(sm));
            }
            else if (sm.type == MessageType.ServiceRequest.code) {
                result.add(new ServiceRequest(sm));
            }
            else if (sm.type == MessageType.ChangeRequest.code) {
                result.add(new ChangeRequest(sm));
            }
            else if (sm.type == MessageType.CancelationRequest.code) {
                result.add(new CancellationRequest(sm));
            }
        }

        return result;
    }

    public void confirmService(long drakeId, int recipientId, float value, Provider provider) throws Exception {

        // [{"$id":"1","Id":0,"ClientGuid":"91ee69d4-2605-4c39-999a-cbdbe1a9eb0a","SenderId":96,"RecipientId":79,"Acknowledgment":true,"Type":1,"DrakeId":589,"ExternalId":"12345","RebatesByReuse":0.0,"SumWithValuesFromPreviousTreatment":false,"ProviderName":"3MARC","ProviderId":"1","Value":100.0,"Fees":0.0,"Commission":0.0,"ServiceOrder":[]}]
        ConfirmService cmd = new ConfirmService();
        cmd.senderId = this.myId;
        cmd.acknowledgment = true;
        cmd.recipientId = recipientId;
        cmd.value = value;
        cmd.providerId = provider.id;
        cmd.providerName = provider.name;
        cmd.drakeId = drakeId;
        cmd.externalId = "12345";
        cmd.reservationNumber = "111";
        cmd.start = LocalDateTime.now();
        cmd.end = LocalDateTime.now();

        ArrayList<ConfirmService> cmds = new ArrayList<ConfirmService>();
        cmds.add(cmd);

        String response = request(cmds, "Send");

        System.out.println(response);
    }

    private String request(Object command, String path) throws Exception {

        // https://www.baeldung.com/httpclient-post-http-request


        String body = gson.toJson(command);

        String date = getDateRFC1123();
        String userAgent = "Sample JAVA";
        String contentType = "application/json";
        String senderId = Integer.toString(this.myId);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", contentType);
        headers.put("Date", date);
        headers.put("User-Agent", userAgent);
        headers.put("x-Drake-SenderId", senderId);

        String signature = generateSignature(body, "POST", headers);

        String url = endpoint + "/MessageService/"+path;

        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);


        StringEntity entity = new StringEntity(body);
        httpPost.setEntity(entity);

         httpPost.setHeader("Content-Type", contentType);
         httpPost.setHeader("Date", date);
         httpPost.setHeader("User-Agent", userAgent);
         httpPost.setHeader("x-Drake-SenderId", senderId);
         httpPost.setHeader("x-Drake-Signature", signature);
         httpPost.setHeader("Accept", "*/*");

        CloseableHttpResponse response = client.execute(httpPost);



        String bodyAsString = EntityUtils.toString(response.getEntity());





        client.close();







        return bodyAsString;
    }

    private String getDateRFC1123() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(calendar.getTime());
    }

    private String generateSignature(String body, String method, HashMap<String, String> headers) throws Exception {
        StringBuilder dataToSign = new StringBuilder();

        dataToSign.append(method).append("\n");

        String[] headersToSign = {"Content-Type", "Date", "User-Agent", "x-Drake-SenderId" };

        for (int i = 0; i <  headersToSign.length; i++)
        {
            String headerName = headersToSign[i].toLowerCase();
            String headerValue = headers.get(headersToSign[i]);
            dataToSign.append(headerName).append(":").append(headerValue).append("\n");
        }

        dataToSign.append("\n");

        dataToSign.append(body);

        String signature = SecurityHelper.signature(this.privateKey, dataToSign.toString());

        return signature;
    }

}
