package com.sapiensia.sample.valueObjects;

import java.util.ArrayList;
import java.util.List;

public class Participant {

    public int drakeId;
    public String name;
    public int gender;
    public String identity;
    public String CPF;
    public String passport;
    public String tels;
    public String costCenter;
    public String email;
    public List<ParticipantDetail> Details = new ArrayList<ParticipantDetail>();

}
