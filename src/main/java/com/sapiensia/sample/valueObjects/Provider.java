package com.sapiensia.sample.valueObjects;

public class Provider {

    public Provider() {

    }

    public Provider(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public String id;
    public String name;
}
