package com.sapiensia.sample.valueObjects;

public class Address {

    public String name;
    public String street;
    public String number;
    public String complement;
    public String district;
    public String city;
    public String state;
    public String country;
    public String zip;
    public Double latitude;
    public Double longitude;
    public String IATA;
}
