package com.sapiensia.sample.valueObjects;

public class ServiceOrder {

    /// <summary>
    ///     Código DRAKE do participante da necessidade de logística.
    /// </summary>
    public long participantDrakeId;

    /// <summary>
    ///     Ordem de origem, números menores saem primeiro.
    /// </summary>

    public int pickupOrder;

    /// <summary>
    ///     Ordem de destino, números menores chegam primeiro.
    /// </summary>

    public int deliveryOrder;
}
