package com.sapiensia.sample.valueObjects;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class LogisticNeed {

    public int drakeId;
    public LocalDateTime start;
    public LocalDateTime end;
    public Address origin;
    public Address destination;
    public LogisticNeedType type;
    public String subtype;
    public String comments;
    public int pickupOrder;
    public int deliveryOrder;
    public Participant participant;
    public List<LogisticNeed> Associated = new ArrayList<LogisticNeed>();

}
