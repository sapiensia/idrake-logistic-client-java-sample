package com.sapiensia.sample.utils;

import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class SecurityHelper {

    public static String signature(String privateKey, String content) throws Exception
    {
        java.security.Signature signature = java.security.Signature.getInstance("SHA512withRSA");

        RSAPrivateKey rsa = getPrivateKey(privateKey);

        signature.initSign(rsa);
        signature.update(content.getBytes("UTF-8"));

        byte[] signed = signature.sign();

        String assinatura = Base64.getEncoder().encodeToString(signed);

        return assinatura;
    }

    public static RSAPrivateKey getPrivateKey(String key) throws Exception
    {

        String privateKeyPEM = key
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replaceAll("\n", "")
                .replace("-----END PRIVATE KEY-----", "");

        byte[] encoded = Base64.getDecoder().decode(privateKeyPEM);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);

    }
}
