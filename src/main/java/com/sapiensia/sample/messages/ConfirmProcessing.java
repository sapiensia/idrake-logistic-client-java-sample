package com.sapiensia.sample.messages;

public class ConfirmProcessing extends Message {
    public ConfirmProcessing(ServiceMessage serviceMessage) {
        this.receivedMessageId = serviceMessage.receivedMessageId;
    }

    public long receivedMessageId;
}
