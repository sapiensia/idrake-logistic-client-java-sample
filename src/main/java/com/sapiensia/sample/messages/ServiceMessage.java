package com.sapiensia.sample.messages;

import com.sapiensia.sample.valueObjects.Address;
import com.sapiensia.sample.valueObjects.LogisticNeed;

import java.time.LocalDateTime;

public class ServiceMessage {
    public long id;
    public int senderId;
    public int type;
    public long receivedMessageId;
    public long drakeId;
    public LocalDateTime sent;
    public String senderUserName;
    public LogisticNeed logisticNeed;


    public LocalDateTime start;
    public LocalDateTime end;
    public Address origin;
    public Address destination;


}
