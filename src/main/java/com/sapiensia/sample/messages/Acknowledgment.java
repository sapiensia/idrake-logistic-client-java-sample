package com.sapiensia.sample.messages;

public class Acknowledgment extends Message {

    public Acknowledgment(ServiceMessage serviceMessage) {
        this.receivedMessageId = serviceMessage.receivedMessageId;
    }

    public long receivedMessageId;
}
