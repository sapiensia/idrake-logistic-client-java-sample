package com.sapiensia.sample.messages;


public class CancellationRequest extends Message {

    public CancellationRequest(ServiceMessage serviceMessage)
    {
        this.id = serviceMessage.id;
        this.drakeId = serviceMessage.drakeId;
        this.senderId = serviceMessage.senderId;
    }

    public long drakeId;
}
