package com.sapiensia.sample.messages;

import com.sapiensia.sample.valueObjects.MessageType;

public class GetUnconfirmedMessage {

    public int type = MessageType.GetUnconfirmedServiceRequest.code;
    public int recipientId;
}
