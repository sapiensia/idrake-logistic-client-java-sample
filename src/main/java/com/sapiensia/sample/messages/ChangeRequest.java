package com.sapiensia.sample.messages;

import com.sapiensia.sample.valueObjects.LogisticNeed;

public class ChangeRequest extends Message {

    public ChangeRequest(ServiceMessage serviceMessage)
    {
        this.id = serviceMessage.id;
        this.drakeId = serviceMessage.drakeId;
        this.senderId = serviceMessage.senderId;
        this.logisticNeed = serviceMessage.logisticNeed;
    }

    public long drakeId;
    public LogisticNeed logisticNeed;
}
