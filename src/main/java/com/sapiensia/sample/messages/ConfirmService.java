package com.sapiensia.sample.messages;

import com.sapiensia.sample.valueObjects.Address;
import com.sapiensia.sample.valueObjects.MessageType;
import com.sapiensia.sample.valueObjects.ServiceOrder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ConfirmService {

    public int type = MessageType.ConfirmService.code;

    public int senderId;
    public int recipientId;
    public boolean acknowledgment;
    public String reservationNumber;
    public String trackingLink;
    public LocalDateTime start;
    public LocalDateTime end;
    public Address origin;
    public Address destination;
    public String externalId;
    public long drakeId;



    public List<ServiceOrder> serviceOrder = new ArrayList<ServiceOrder>();

    /// <summary>
    ///     Provedor de serviço.
    /// </summary>
    public String providerId;
    public String providerName;

    /// <summary>
    ///     Valor APENAS do serviço.
    /// </summary>
    public float value;

    /// <summary>
    ///     Taxas
    /// </summary>
    public float fees;

    /// <summary>
    ///     Comissão.
    /// </summary>
    public float commission;
}
