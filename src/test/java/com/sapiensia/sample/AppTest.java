package com.sapiensia.sample;

import com.sapiensia.sample.valueObjects.Provider;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() throws Exception {
        String privateKey = "SUA CHAVE PRIVADA AQUI";
        int id = 1111111111; // SEU CODIGO AQUI
        String endpoint = "https://logistic-integrator-tst.drake.bz";
        iDrakeLogisticClient client = new iDrakeLogisticClient(endpoint, privateKey, id);

       client.readMessages();

        client.confirmService(589, 79, 100, new Provider("1", "3MARC"));

    }
}
